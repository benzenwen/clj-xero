(ns clj-xero.core
  (:require
    [clojure.string :as string]
    [clojure.data.json :as json]
    [clj-http.client :as clj-http]

    [oauth.client :as oauth.client]
    [inflections.core :as inflections]
    [throttler.core :refer [throttle-fn]]
    [org.tobereplaced.lettercase :as letter-case])
  (:import (com.google.api.client.util.escape PercentEscaper)))

(defn public-consumer
  "Returns an oauth consumer, usable for obtaining authorisation for a public user to their xero organisation."
  [key secret]
  (oauth.client/make-consumer key secret
                              "https://api.xero.com/oauth/RequestToken"
                              "https://api.xero.com/oauth/AccessToken"
                              "https://api.xero.com/oauth/Authorize"
                              :hmac-sha1))

(defn authorisation-request-token
  [consumer callback-url]
  (let [request-token (oauth.client/request-token consumer callback-url)]
    (assoc request-token :authorisation-url
                         (->> request-token
                              (:oauth_token)
                              (oauth.client/user-approval-uri consumer)))))

(def base-api-url "https://api.xero.com/api.xro/2.0/")
(def ^:dynamic *base-api-url* base-api-url)

(defmacro with-api-url
  "Allows for configuring the api url which is used for accessing the assets.
  Usually not needed, but some API's are still on older URL's"
  [api-url & body]
  `(binding [*base-api-url* ~api-url]
     ~@body))

(defn public-authorised-credentials
  "Create credentials for a pre-authorised public user"
  [consumer request-token verifier-code]
  {:pre [(string? verifier-code) (map? request-token) consumer]}
  (-> (oauth.client/access-token consumer request-token verifier-code)
      (assoc :consumer consumer)))

(defn partner-authorised-credentials
  "Create credentials for a pre-authorised partner user"
  [consumer request-token verifier-code]
  {:pre [(string? verifier-code) (map? request-token) consumer]}
  (-> (oauth.client/access-token consumer request-token verifier-code)
      (assoc :consumer consumer)))

(defn partner-authorised-credentials-refresh
  "Create credentials for a partner user with an existing token refresh"
  [consumer request-token verifier-code]
  {:pre [(map? request-token) consumer]}
  (-> (oauth.client/refresh-token consumer request-token verifier-code)
      (assoc :consumer consumer)))

(defn private-credentials
  [key secret private-key]
  {:pre [(string? private-key)]}
  {:consumer        (oauth.client/make-consumer key
                                                private-key
                                                "https://api.xero.com/api.xro/2.0/oauth/RequestToken"
                                                "https://api.xero.com/api.xro/2.0/oauth/AccessToken"
                                                "https://api.xero.com/api.xro/2.0/oauth/Authorise"
                                                :rsa-sha1)
   :consumer-secret secret})

(defn partner-consumer [key private-key]
  (oauth.client/make-consumer key
                              private-key
                              "https://api.xero.com/oauth/RequestToken"
                              "https://api.xero.com/oauth/AccessToken"
                              "https://api.xero.com/oauth/Authorize"
                              :rsa-sha1))

(def ^:dynamic *current-credentials*)

(defmacro with-credentials
  [credentials & body]
  `(binding [*current-credentials* ~credentials]
     ~@body))

(def action-keywords
  {:get         "get"
   :get-all     "get"
   :get-by-guid "get"
   :post        "add-or-update"
   :put         "add"
   :delete      "delete"
   :attachments "Attachments"})

(defn- format-entity-name
  [entity-name]
  (letter-case/lower-hyphen (name entity-name)))

(defn- fn-name
  [action endpoint & {:keys [postfix prefix]}]
  (let [action-name (action-keywords action)
        endpoint    (format-entity-name endpoint)
        punc        (if (#{:put :post :delete} action) "!" "")]
    (symbol (format "%s-%s%s%s%s%s%s"
                    action-name
                    (if prefix (letter-case/lower-hyphen (name prefix)) "")
                    (if prefix "-" "")
                    endpoint punc
                    (if postfix "-" "")
                    (if postfix (letter-case/lower-hyphen (name postfix)) "")))))

(defn- make-credentials
  [{:keys [consumer oauth_token oauth_token_secret consumer-secret]} method for-url & [params]]
  (oauth.client/credentials consumer
                            (or oauth_token (:key consumer))
                            (or oauth_token_secret consumer-secret)
                            method
                            for-url
                            (or params {})))

(def escaper (PercentEscaper. "-_.~" false))
(defn make-credentials-header-entry
  [key value escapeer]
  (let [key   (str key)
        value (str value)]
    (when (and (not (string/blank? key))
               (not (string/blank? value)))
      (str (.escape escaper key) "=\"" (.escape escaper value) "\""))))

(defn make-credentials-header
  [credentials]
  (let [header-value (map
                       (fn [[key value]]
                         (make-credentials-header-entry (name key) value escaper))
                       credentials)
        header-value (string/join "," header-value)
        header-value (str "Oauth " header-value)]
    {"Authorization" header-value}))

(defn- lowername-keys [params]
  (zipmap (map (comp letter-case/lower-keyword name) (keys params)) (vals params)))

(defn- private-credentials? [credentials]
  (contains? credentials :consumer))

(defn- public-or-private-credentials? [credentials]
  (every? true? (map #(contains? credentials %)
                     #{:consumer :xero_org_muid :oauth_token :oauth_token_secret})))

(defn valid-credentials-for-auth-type?
  "Checks all required keys are present and entrust store is provided if using the partner api"
  [credentials]
  (or (public-or-private-credentials? credentials)
      (private-credentials? credentials)))

(defn- do-request-for-auth-type
  [request-fn url headers query-params body input-stream]
  (request-fn url (merge {:headers headers}
                         (when query-params
                           {:query-params query-params})
                         (if body
                           {:body          (str body)
                            :body-encoding "UTF-8"
                            :content-type  "text/json"}
                           (if (map? input-stream)
                             input-stream
                             {:body input-stream}))
                         (when (= "application/pdf" (headers "Accept"))
                           {:as :stream}))))

(defn- do-request*
  [request-method endpoint credentials
   & {:keys [params body guid modified-since attachments filename input-stream page offset result-fn body-fn accept-type]}]
  {:pre [(map? credentials)
         (valid-credentials-for-auth-type? credentials)]}

  (let [url-base    (str (or (:base-api-url credentials)
                             *base-api-url*
                             base-api-url))
        url-base    (if (string/ends-with? url-base "/")
                      url-base
                      (str url-base "/"))
        url         (str url-base
                         (letter-case/capitalized-name endpoint)
                         (when guid
                           (str "/" guid))
                         (when attachments
                           (str "/" attachments))
                         (when filename
                           (str "/" filename)))
        request-fn  ({:GET clj-http/get :POST clj-http/post :PUT clj-http/put :DELETE clj-http/delete} request-method)
        params      (merge (lowername-keys params) (when page {:page page}) (when offset {:offset offset}))
        user-agent  (:user-agent credentials)
        credentials (make-credentials credentials request-method url params)
        headers     (merge {"Accept" (or accept-type "application/json")}
                           (make-credentials-header credentials)
                           (when user-agent {"User-Agent" user-agent})
                           (when body {"Content-Type" "application/x-www-form-urlencoded"})
                           (when input-stream {"Content-Type" "application/pdf"})
                           (when modified-since {"If-Modified-Since" ""}))
        body        (when body (json/write-str body :key-fn (comp letter-case/capitalized name)))
        response    (do-request-for-auth-type request-fn url headers params body input-stream)]
    (case (:status response)
      200 (let [body-fn   (or body-fn
                              (fn [resp] (json/read-str (:body resp) :key-fn letter-case/lower-hyphen-keyword)))
                result-fn (or result-fn
                              (fn [json-resp]
                                (let [resp-key (if attachments
                                                 (keyword (letter-case/lower attachments))
                                                 endpoint)]
                                  (-> json-resp resp-key))))]
            (result-fn (body-fn response)))
      404 nil
      (throw (ex-info "Error while executing Xero API request." response)))))

(def ^:private do-request
  (throttle-fn do-request* 60 :minute))

(defmulti client-fns
  (fn [action _ _] action))

(defn- get-all-by-page*
  [fetch-fn]
  (loop [entities     (fetch-fn :page 1)
         last-results entities
         page         1]
    (if (not= 100 (count last-results))
      entities
      (let [next-page (fetch-fn :page (inc page))]
        (recur (concat entities next-page)
               next-page
               (inc page))))))

(def ^:private get-all-by-page
  (throttle-fn get-all-by-page* 1 :second))

(defn- get-all-by-offset*
  [fetch-fn endpoint]
  (let [offset-kw (-> (inflections/singular endpoint) name (str "-number") keyword)]
    (loop [entities     (fetch-fn)
           last-results entities]
      (if (not= 100 (count last-results))
        entities
        (let [next-results (fetch-fn :offset (apply max (map offset-kw last-results)))]
          (recur (concat entities next-results)
                 next-results))))))

(def ^:private get-all-by-offset
  (throttle-fn get-all-by-offset* 1 :second))

(defn- client-fns-get-all
  [action endpoint {:keys [get-all-paging-type private? api-url result-fn body-fn]}]
  {;; get all
   (with-meta
     (fn-name action (inflections/plural endpoint) :prefix :all)
     {:doc      (str "Retrieve a seq of all " (name (name endpoint)))
      :private  private?
      :arglists [[] ['credentials]]})
   (fn f
     ([] (f *current-credentials*))
     ([credentials]
      (let [fetch-data (partial do-request :GET endpoint credentials
                                :result-fn result-fn
                                :body-fn body-fn)]
        (with-api-url api-url
          (case get-all-paging-type
            :page
            (get-all-by-page fetch-data)

            :offset
            (get-all-by-offset fetch-data endpoint)

            (fetch-data))))))

   ;; get all by params only.
   (with-meta
     (fn-name action (inflections/plural endpoint) :prefix :all :postfix :by-params)
     {:doc      (str "Retrieve the " (name (inflections/plural endpoint)) " matching the given params")
      :private  private?
      :arglists [['params] ['credentials 'params]]})
   (fn f
     ([params]
      (f *current-credentials* params))
     ([credentials params]
      (let [fetch-data (partial do-request :GET endpoint credentials
                                :params params
                                :result-fn result-fn
                                :body-fn body-fn)]
        (with-api-url api-url
          (case get-all-paging-type
            :page
            (get-all-by-page fetch-data)

            :offset
            (get-all-by-offset fetch-data endpoint)

            (fetch-data))))))})

(defn- client-fns-get-by-guid
  [action endpoint {:keys [private? api-url result-fn body-fn accept-type]}]
  {;; get by id
   (with-meta
     (fn-name action (inflections/singular endpoint) :postfix :by-guid)
     {:doc      (str "Retrieve the " (name (inflections/singular endpoint)) " by the given guid")
      :private  private?
      :arglists [['guid] ['credentials 'guid]]})
   (fn f
     ([guid] (f *current-credentials* guid))
     ([credentials guid]
      (with-api-url api-url
        (first (do-request :GET endpoint credentials
                           :guid guid
                           :body-fn body-fn
                           :result-fn result-fn
                           :accept-type accept-type)))))

   ;;get by id with filter params
   (with-meta
     (fn-name action (inflections/singular endpoint) :postfix :by-guid-and-params)
     {:doc      (str "Retrieve the " (name (inflections/singular endpoint)) " by the given guid, with params")
      :private  private?
      :arglists [['guid 'params] ['credentials 'guid 'params]]})
   (fn f
     ([guid params] (f *current-credentials* guid params))
     ([credentials guid params]
      (with-api-url api-url
        (first (do-request :GET endpoint credentials
                           :guid guid
                           :params params
                           :body-fn body-fn
                           :result-fn result-fn)))))
   })

(defn- decorate-ents
  "Decorate the given entity or entities so that the data structure is in an acceptable
  format for xero json."
  [endpoint ents]
  {:pre [(keyword? endpoint) (coll? ents)]}
  {endpoint ents})

(defn- client-fns-put
  [action endpoint {:keys [api-url result-fn body-fn]}]
  {(with-meta
     (fn-name action endpoint)
     {:doc      (str "Add new " (name endpoint))
      :arglists [[(symbol (name endpoint))] ['credentials (symbol (name endpoint))]]})
   (fn f
     ([ents] (f *current-credentials* ents))
     ([credentials ents]
      {:pre [(seq ents)]}
      (let [decorated-ent (decorate-ents endpoint ents)]
        (with-api-url api-url
          (do-request :PUT endpoint credentials
                      :body decorated-ent
                      :body-fn body-fn
                      :result-fn result-fn)))))})

(defn- client-fns-post
  [action endpoint {:keys [api-url result-fn body-fn]}]
  {(with-meta
     (fn-name action endpoint)
     {:doc      (str "Add a new or update the given " (name endpoint))
      :arglists [[(symbol (name endpoint))] ['credentials (symbol (name endpoint))]]})
   (fn f
     ([ent] (f *current-credentials* ent))
     ([credentials ents]
      {:pre [(seq ents)]}
      (let [decorated-ent (decorate-ents endpoint ents)]
        (with-api-url api-url
          (do-request :POST endpoint credentials
                      :body decorated-ent
                      :body-fn body-fn
                      :result-fn result-fn)))))})

(defn- client-fns-attachments
  [action endpoint {:keys [api-url result-fn body-fn]}]
  {;; get all attachments
   (with-meta
     (fn-name :get (inflections/singular endpoint) :postfix :attachments-by-guid)
     {:doc      (str "Get all attachments for an " (name (inflections/singular endpoint)) " " (name action) "by the given guid")
      :arglists [['guid] ['credentials 'guid]]})
   (fn f
     ([guid] (f *current-credentials* guid))
     ([credentials guid]
      (with-api-url api-url
        (do-request :GET endpoint credentials
                    :guid guid
                    :attachments "Attachments"
                    :body-fn body-fn
                    :result-fn result-fn))))

   ;;TODO get the contents of an attachment. Not working because bytestream is returned

   ;; Create attachment for endpoint
   (with-meta
     (fn-name :put (inflections/singular action))
     {:doc      (str "Create an attachment for an " (name (inflections/singular endpoint)))
      :arglists [['guid 'filename 'input] ['credentials 'guid 'filename 'input]]})
   (fn f
     ([guid filename input] (f *current-credentials* guid filename input))
     ([credentials guid filename input]
      (with-api-url api-url
        (do-request :PUT endpoint credentials
                    :guid guid
                    :attachments "Attachments"
                    :filename filename
                    :input-stream input
                    :body-fn body-fn
                    :result-fn result-fn))))})

(defmethod client-fns :get-all
  [action endpoint options]
  (client-fns-get-all action endpoint options))

(defmethod client-fns :get-by-guid
  [action endpoint options]
  (client-fns-get-by-guid action endpoint options))

(defmethod client-fns :get
  [_ endpoint options]
  (merge
    (client-fns-get-all :get-all endpoint options)
    (client-fns-get-by-guid :get-by-guid endpoint options)))

(defmethod client-fns :put
  [action endpoint options]
  (client-fns-put action endpoint options))

(defmethod client-fns :post
  [action endpoint options]
  (client-fns-post action endpoint options))

(defmethod client-fns :attachments
  [action endpoint options]
  (client-fns-attachments action endpoint options))

(defmethod client-fns :default
  [_ _ _]
  nil)

(defn set-client!
  [endpoint actions ns & {:as options}]
  {:pre [(keyword? endpoint) (set? actions)]}
  (doseq [action actions]
    (doseq [[sym f] (client-fns action endpoint options)]
      (intern ns sym f))))