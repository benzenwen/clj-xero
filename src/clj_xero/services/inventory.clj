(ns clj-xero.services.inventory
  (:require [clj-xero.core :as core]))

(core/set-client! :items #{:get :post :put :delete} *ns*)

