(ns clj-xero.services.invoices.pdf
  (:require [clj-xero.core :as core]))

(core/set-client! :invoices #{:get-by-guid} *ns*
                  :accept-type "application/pdf"
                  :body-fn (fn [r]
                             {:content {:stream (:body r)
                                        :length (-> r :headers (get "Content-Length") Integer/parseInt)}})
                  :result-fn list)
