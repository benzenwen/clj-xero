(ns clj-xero.services.manual-journals
  (:require [clj-xero.core :as core]))

(core/set-client! :manual-journals #{:get :post} *ns* :get-all-paging-type :offset)
