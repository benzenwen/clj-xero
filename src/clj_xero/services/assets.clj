(ns clj-xero.services.assets
  (:require
    [clj-xero.core :as core]
    [clojure.data.json :as json]
    [org.tobereplaced.lettercase :as letter-case]))

(def api-url "https://api.xero.com/assets.xro/1.0/")

(core/set-client!
  :assets
  #{:get-all}
  *ns*
  :private? true
  :get-all-paging-type :page
  :result-fn :items
  :api-url api-url)

(core/set-client!
  :assets
  #{:get-by-guid}
  *ns*
  ;; The body of the new API comes back with 'items', but the old API doesn't so the framework will expect a sequence
  ;; so wrap the result.
  :result-fn (fn [r] [r])
  :api-url api-url)

(core/set-client!
  :asset-types
  #{:get-all}
  *ns*
  :result-fn identity
  :api-url api-url)

(defn get-all-assets-by-status
  ([asset-status]
   (get-all-assets-by-status core/*current-credentials* asset-status))
  ([credentials asset-status]
   (let [status-code (letter-case/upper-name asset-status)
         params      {:status status-code}]
     (get-all-assets-by-params credentials params))))