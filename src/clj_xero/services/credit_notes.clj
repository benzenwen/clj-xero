(ns clj-xero.services.credit-notes
  (:require [clj-xero.core :as core]))

(core/set-client! :credit-notes #{:get :post :put :delete :attachments} *ns*
                  :get-all-paging-type :page)
