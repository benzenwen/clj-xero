(ns clj-xero.services.payroll.employees
  (:require [clj-xero.core :as core]))


(def api-url "https://api.xero.com/payroll.xro/1.0/")

(core/set-client!
  :employees
  #{:get}
  *ns*
  :result-fn :employees
  :api-url api-url)
