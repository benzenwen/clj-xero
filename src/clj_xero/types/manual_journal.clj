(ns clj-xero.types.manual-journal)

(def status-codes {:draft   "DRAFT"
                   :posted  "POSTED"
                   :deleted "DELETED"
                   :voided  "VOIDED"})

(def line-amount-types {:exclusive "Exclusive"
                        :inclusive "Inclusive"
                        :no-tax    "NoTax"})
