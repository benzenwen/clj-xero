(ns clj-xero.types.contact-status)

(def active
  "The Contact is active and can be used in transactions\t"
  "ACTIVE")

(def archived
  "The Contact is archived and can no longer be used in transactions\t"
  "ARCHIVED")

(def gdpr-request
  "The Contact is the subject of a GDPR erasure request and can no longer be used in tranasctions"
  "GDPRREQUEST")
