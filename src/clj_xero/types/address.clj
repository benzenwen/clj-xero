(ns clj-xero.types.address)

(def post-box
  "The default mailing address for invoices."
  "POBOX")

(def street
  "The street address."
  "STREET")

(def delivery
  "Read-only via the GET Organisation endpoint (if set). The delivery address of the Xero organisation.
  DELIVERY address type is not valid for Contacts."
  "DELIVERY")
